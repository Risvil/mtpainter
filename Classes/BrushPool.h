#ifndef BRUSHPOOL_H
#define BRUSHPOOL_H

#include "cocos2d.h"

class BrushPool
{
public:
	BrushPool();
	~BrushPool();

	cocos2d::Sprite* getBrush();

private:
	static const int POOL_SIZE = 100;
	cocos2d::Sprite* pool[POOL_SIZE];
	int nextAvailable;
};

#endif // BRUSHPOOL_H