#include "HelloWorldScene.h"
#include "KimSprite.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
		origin.y + closeItem->getContentSize().height / 2));

	// Add kim close button, size to match closeItem from menu
	auto closeButton = KimSprite::create();
	closeButton->setContentSize(closeItem->getContentSize());
	closeButton->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
		origin.y + closeItem->getContentSize().height / 2));

	closeButton->setOnTouchUpDelegate([](const kim::TouchEvent& e) {
		Director::getInstance()->end();
	});
	addChild(closeButton, 2);


	// add a "new file" icon to clear the background
	auto newFileItem = MenuItemImage::create(
											"NewFileNormal.png",
											"NewFileSelected.png",
											CC_CALLBACK_1(HelloWorld::menuNewFileCallback, this));
    
	newFileItem->setPosition(Vec2(origin.x + visibleSize.width - newFileItem->getContentSize().width * 2,
		origin.y + newFileItem->getContentSize().height / 2));

	// Add kim new button, size to match closeItem from menu
	auto newFileButton = KimSprite::create();
	newFileButton->setContentSize(newFileItem->getContentSize());
	newFileButton->setPosition(Vec2(origin.x + visibleSize.width - newFileItem->getContentSize().width / 2,
		origin.y + newFileItem->getContentSize().height / 2));

	newFileButton->setOnTouchUpDelegate([this](const kim::TouchEvent& e) {
		this->onNewFile();
	});
	addChild(newFileButton, 2);


    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
	menu->addChild(newFileItem);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 2);

    /////////////////////////////
    // 3. add your codes below...

	// Create Drawable canvas
	m_pTarget = CCRenderTexture::create(visibleSize.width, visibleSize.height);
	m_pTarget->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	addChild(m_pTarget, 1);

	// add background
	m_spriteBackground = KimSprite::create("background.jpg");

	// Setup spriteBackround delegates
	m_spriteBackground->setOnTouchDownDelegate(std::bind(&HelloWorld::onTouchDown, this, std::placeholders::_1));
	m_spriteBackground->setOnTouchMoveDelegate(std::bind(&HelloWorld::onTouchMove, this, std::placeholders::_1));
	m_spriteBackground->setOnTouchUpDelegate(std::bind(&HelloWorld::onTouchUp, this, std::placeholders::_1));

	m_spriteBackground->setContentSize(visibleSize);
	m_spriteBackground->setPosition(visibleSize.width / 2, visibleSize.height / 2);
	this->addChild(m_spriteBackground, 0);

	// init KIM
	m_pEngine = kim::IEngine::CreateEngine();
	if (m_pEngine->Init())
	{
		// Register touchable regions
		m_pEngine->RegisterRegion(newFileButton, kim::IEngine::Priority::FOREGROUND);
		m_pEngine->RegisterRegion(closeButton, kim::IEngine::Priority::FOREGROUND);
		m_pEngine->RegisterRegion(m_spriteBackground, kim::IEngine::Priority::BACKGROUND);
		m_pEngine->Run();
	}
    
    return true;
}

void HelloWorld::menuNewFileCallback(cocos2d::Ref* pSender)
{
	onNewFile();
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::onTouchDown(const kim::TouchEvent& e)
{
	Vec2 touch(e.x, e.y);
	if (m_spriteBackground != nullptr)
	{
		auto touchLocal = m_spriteBackground->getLocalPosition(touch);
		addTouch(e);
	}
}

void HelloWorld::onTouchMove(const kim::TouchEvent& e)
{
	Vec2 touch(e.x, e.y);
	if (m_spriteBackground != nullptr)
	{
		auto touchLocal = m_spriteBackground->getLocalPosition(touch);
		addTouch(e);
	}
}

void HelloWorld::onTouchUp(const kim::TouchEvent& e)
{

}

void HelloWorld::draw(cocos2d::Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	m_pTarget->begin();

	m_touchesMutex.lock();
	for (auto& touch : m_Touches)
	{
		auto touchPosition = Vec2(touch.x, touch.y);
		// Do not transform mouse cursor
		if (touch.pointID != 0)
		{
			touchPosition = m_spriteBackground->getLocalPosition(touchPosition);
		}

		CCLOG("Drawing - Touch ID: %d, X: %f, Y: %f", touch.pointID, touchPosition.x, touchPosition.y);

		// Cocos2D does not allow to visit an sprite more than once per render call, so we create an sprite for each touch to draw
		auto brushSprite = m_brushes.getBrush();
		brushSprite->setPosition(touchPosition);
		brushSprite->visit();
	}

	m_Touches.clear();
	m_touchesMutex.unlock();

	m_pTarget->end();

	cocos2d::Layer::draw(renderer, transform, flags);
}

void HelloWorld::onNewFile()
{
	// Clear texture in full transparent black color
	m_pTarget->clear(0, 0, 0, 0);
}

void HelloWorld::onExit()
{
	Layer::onExit();

	m_pEngine->Shutdown();
}

void HelloWorld::addTouch(const kim::TouchEvent& touch)
{
	m_touchesMutex.lock();
	m_Touches.push_back(touch);
	m_touchesMutex.unlock();
}
