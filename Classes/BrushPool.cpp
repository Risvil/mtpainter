#include "BrushPool.h"


BrushPool::BrushPool()
{
	for (int i = 0; i < POOL_SIZE; ++i)
	{
		pool[i] = cocos2d::Sprite::create("largeBrush.png");
		pool[i]->retain();
	}

	nextAvailable = -1;
}

BrushPool::~BrushPool()
{
	for (int i = 0; i < POOL_SIZE; ++i)
	{
		pool[i]->release();
	}
}

cocos2d::Sprite* BrushPool::getBrush()
{
	nextAvailable = ++nextAvailable % POOL_SIZE;
	return pool[nextAvailable];
}