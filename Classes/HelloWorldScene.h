#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include <vector>
#include <mutex>
#include <memory>
#include "kim.h"
#include "BrushPool.h"


class KimSprite;
class HelloWorld : public cocos2d::Layer
{
public:
	// there's no 'id' in cpp, so we recommend returning the class instance pointer
	static cocos2d::Scene* createScene();

	// Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
	virtual bool init();

	// a selector callback
	void menuNewFileCallback(cocos2d::Ref* pSender);
	void menuCloseCallback(cocos2d::Ref* pSender);

	void onTouchDown(const kim::TouchEvent& e);
	void onTouchMove(const kim::TouchEvent& e);
	void onTouchUp(const kim::TouchEvent& e);

	void drawBrush(const cocos2d::Vec2& position);

	virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

	void onNewFile();
	virtual void onExit() override;

	// implement the "static create()" method manually
	CREATE_FUNC(HelloWorld);

private:
	void addTouch(const kim::TouchEvent& touch);

private:
	cocos2d::RenderTexture* m_pTarget;
	KimSprite* m_spriteBackground;
	std::vector<kim::TouchEvent> m_Touches;
	std::mutex m_touchesMutex;
	BrushPool m_brushes;

	std::shared_ptr<kim::IEngine> m_pEngine;
};

#endif // __HELLOWORLD_SCENE_H__
