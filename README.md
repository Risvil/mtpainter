Welcome to Multi-Touch Painter!
=====================

### Disclaimer: This is a work in progress. ###

**Multi-Touch Painter** is a sample application implemented using Keep-In Touch (https://bitbucket.org/Risvil/keepinmove). It is a simple finger painting application. Multiple fingers can be used to paint the surface simultaneously.

----------